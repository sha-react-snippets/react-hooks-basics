import React, { useState, useEffect } from 'react';
import axios from 'axios';

import List from '../List';

const App = () => {
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        setUsers(response.data);
      } catch (err) {
        // Handle error.
      }
    })();

    setIsLoading(false);
  }, [isLoading]);

  return (
    isLoading
      ? <h3>Loading...</h3>
      : <List users={users} />
  );
};

export default App;
