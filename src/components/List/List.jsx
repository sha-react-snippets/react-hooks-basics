import React from 'react';
import PropTypes from 'prop-types';

const List = (props) => {
  const { users } = props;

  return (
    <ul>
      {
        users.map((user) => (
          <li>
            <h3>{user.email}</h3>
          </li>
        ))
      }
    </ul>
  );
};

List.propTypes = {
  users: PropTypes.shape({
    length: PropTypes.number,
    map: PropTypes.func,
  }).isRequired,
};

export default List;
